var mongoose = require('./db');
var ObjectID = require('mongodb').ObjectID;
var WUser = require('./wuser.js');



var orderSchema = new mongoose.Schema({
    out_trade_no: String,
    openid : String,
    total_fee : String,
    orderstatus: String,
    proid: String
}, {
    collection: 'order'
});

var orderModel = mongoose.model('Order', orderSchema);

function Order(out_trade_no,openid, total_fee,orderstatus,proid) {
    this.out_trade_no = out_trade_no;
    this.openid = openid;
    this.total_fee = total_fee;
    this.orderstatus = orderstatus;
    this.proid = proid;
}

module.exports = Order;

//存储一篇文章及其相关信息
Order.prototype.save = function (callback) {
    var date = new Date();
    //存储各种时间格式，方便以后扩展
    var time = {
        year: date.getFullYear(),
        month: date.getFullYear() + "-" + (date.getMonth() + 1),
        day: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate(),
        minute: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +
            date.getHours() + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
    };
    var order = {
        out_trade_no: this.out_trade_no,
        openid: this.openid,
        total_fee: this.total_fee,
        orderstatus: this.orderstatus,
        proid: this.proid,
        time : time
    };

    var newOrder = new orderModel(order);
    //打开数据库
    newOrder.save(function (err, doc) {
        if (err) {
            return callback(err);
        }
        callback(null, doc);
    });
};

//获取一个订单
Order.getOne = function(out_trade_no, callback) {
    orderModel.findOne({"out_trade_no": out_trade_no}, function (err, doc) {
        if (err) {
            return callback(err);
        }
        if (doc) {
            callback(null, doc);
        }
    });
};
//更新订单状态
Order.updateOrderStatus = function(out_trade_no,orderstatus,callback) {
    orderModel.update({out_trade_no: out_trade_no},{
        $set: {"orderstatus": orderstatus}
    },function (err, doc) {
        if (err) {
            return callback(err);
        }
        if (doc) {
            callback(null, doc);
            console.info(doc);
        }
    });
};





