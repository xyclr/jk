var settings = require('../settings');
var Post = require('../models/post.js');
var Comment = require('../models/comment.js');
var WUser = require('../models/wuser.js');
var Card = require('../models/card.js');
var Order = require('../models/order.js');
var url = require('url');
var querystring = require('querystring');
var WXPay = require('weixin-pay');
var fs = require("fs") ;

var API = require('wechat-api');
var api = new API(settings.wx.appid,settings.wx.AppSecret);

var wechat = require('wechat');
var config = {
    token: settings.wx.token,
    appid: settings.wx.appid,
    encodingAESKey: settings.wx.encodingAESKey
};

var wxpay = WXPay({
    appid: settings.wx.appid,
    mch_id: settings.wx.mchId,
    partner_key: settings.wx.partnerkey,
    pfx: fs.readFileSync(settings.wx.pfx)
});


var OAuth = require('wechat-oauth');
var client = new OAuth(settings.wx.appid, settings.wx.AppSecret);

var oauthApi = new OAuth(settings.wx.appid, settings.wx.AppSecret, function (openid, callback) {
    // 传入一个根据openid获取对应的全局token的方法
    fs.readFile(openid +':access_token.txt', 'utf8', function (err, txt) {
        if (err) {return callback(err);}
        callback(null, JSON.parse(txt));
    });
}, function (openid, token, callback) {
    // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
    // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
    // 持久化时请注意，每个openid都对应一个唯一的token!
    fs.writeFile(openid + ':access_token.txt', JSON.stringify(token), callback);
});

module.exports = function (app) {
    // 主页,主要是负责OAuth认真
    app.get('/mall', function(req, res) {
        var url = client.getAuthorizeURL('http://www.sd188.cn/user','','snsapi_userinfo');
        console.info("AuthorizeURL: " + url);
        res.redirect(url);
    })


    app.get('/user', function(req, res) {
        var code = req.query.code;
        client.getAccessToken(code, function (err, result) {
            if(err) {
                console.info(err);
                return false;
            }
            var accessToken = result.data.access_token;
            var openid = result.data.openid;
            console.log('token=' + accessToken);
            console.log('openid=' + openid);
            WUser.get(openid,function(err, wuser){
                console.log('use weixin api get user1: '+ err)
                console.info(wuser);
                if(err || wuser == null){
                    console.log('user is not exist.')
                    client.getUser(openid, function (err, result) {
                        console.log('use weixin api get user2: '+ err)
                        console.log(result)

                        var _user = new WUser({
                            openid: result.openid,
                            nickname: result.nickname,
                            headimgurl: result.headimgurl,
                            city: result.city,
                            province: result.city,
                            country: result.country,
                            sex: result.sex
                        });

                        _user.save(function(err, wuser) {
                            if (err) {
                                console.log('User save error ....' + err);
                            } else {
                                console.log('User save sucess ....' + err);
                                req.session.wuser = wuser;
                                res.redirect('/case');
                            }
                        });

                    });
                } else {
                    console.info("user exist");
                    client.getUser(openid, function (err, result) {
                        console.info("result3:");
                        console.info(result)
                        var _user = new WUser({
                            openid: result.openid,
                            nickname: result.nickname,
                            headimgurl: result.headimgurl,
                            city: result.city,
                            province: result.city,
                            country: result.country,
                            sex: result.sex
                        });
                        req.session.wuser = _user;
                        console.info("save session end:");
                        console.info(req.session.wuser);
                        res.redirect('/case');
                    });

                }
            })

        });
    });

    app.post('/wechat', wechat(config, function (req, res, next) {
        // 微信输入信息都在req.weixin上
        var message = req.weixin;
        if (message.Content === 'diaosi') {
            // 回复屌丝(普通回复)
            res.reply('hehe');
        } else if (message.Content === 'text') {
            //你也可以这样回复text类型的信息
            res.reply({
                content: 'text object',
                type: 'text'
            });
        } else if (message.Content === 'hehe') {
            // 回复一段音乐
            res.reply({
                type: "music",
                content: {
                    title: "来段音乐吧",
                    description: "一无所有",
                    musicUrl: "http://mp3.com/xx.mp3",
                    hqMusicUrl: "http://mp3.com/xx.mp3",
                    thumbMediaId: "thisThumbMediaId"
                }
            });
        } else {
            // 回复高富帅(图文回复)
            res.reply([
                {
                    title: '欢迎您关注身边公益',
                    description: '欢迎您关注身边公益欢迎您关注身边公益欢迎您关注身边公益欢迎您关注身边公益',
                    picurl: 'http://nodeapi.cloudfoundry.com/qrcode.jpg',
                    url: 'http://nodeapi.cloudfoundry.com/'
                }
            ]);
        }
    }));

    app.get('/pay/:_id', checkLogin);
    app.get('/pay/:_id', function(req, res) {
        var _id = req.params._id;
        var arg = url.parse(req.url).query;
        var money = querystring.parse(arg).money;
        var title = querystring.parse(arg).title;
        var out_trade_no =  '' + CurentTime() +Math.random().toString().substr(2, 10);
        wxpay.getBrandWCPayRequestParams({
            openid: req.session.wuser.openid,
            body: '商品支付',
            detail: '商品支付',
            out_trade_no: out_trade_no,
            total_fee: money*100,
            spbill_create_ip: settings.ip,
            notify_url: settings.wx.notifyUrl
        }, function(err, result){
            if(err) {
                res.render(err);
            }
            var _order = new Order(out_trade_no,req.session.wuser.openid, money*100,0,_id);//0 未支付
            _order.save(function(err, doc) {
                if (err) {
                    console.log('Order save error ....' + err);
                } else {
                    console.log('Order save sucess ....' + err);
                }
            });

            res.render('pay/', {
                payargs:result,
                caseinfo : {
                    title :title,
                    money : money
                }
            })
        });
    });

    app.post('/payMsg', checkLogin);
    app.post('/pay/payMsg', wxpay.useWXCallback(function(msg, req, res, next){
        // res.success() 向微信返回处理成功信息，res.fail()返回失败信息。
        //一共要发4个post ？？？？？
        var proid = "";
        var point = "";
        Order.getOne(msg.out_trade_no, function (err, doc) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            proid = doc.proid;
            point = doc.total_fee/100;
            if(doc.orderstatus ==  "0") {
                if(msg.return_code == "SUCCESS") {
                    //更新orderstatus
                    Order.updateOrderStatus(msg.out_trade_no,1,function(err,doc){
                        //更新积分
                        WUser.addPoint(msg.openid,proid,point,function(err,doc){
                            if(err){
                                console.info(err)
                            }
                            console.info("更新成功！")
                        })
                    })
                    res.success();
                } else {
                    //支付失败
                    res.fail();
                }
            } else {
                res.success();
            }
        });

    }));

    app.get('/wechat-api', checkLogin);
    app.get('/wechat-api', function(req,res){
        var menu =  {
            "button":[
                {
                    "type":"click",
                    "name":"今日歌曲",
                    "key":"V1001_TODAY_MUSIC"
                },
                {
                    "name":"菜单",
                    "sub_button":[
                        {
                            "type":"view",
                            "name":"商城",
                            "url":"http://www.sd188.cn/mall"
                        },
                        {
                            "type":"view",
                            "name":"接口测试",
                            "url":"http://www.sd188.cn/wechat-api"
                        },
                        {
                            "type":"click",
                            "name":"赞一下我们",
                            "key":"V1001_GOOD"
                        }]
                }]
        };

        api.createMenu(menu, function(err,result){
            if(err) {
                console.info("err:" + err);
            }
            console.info(result);
        });
        api.getIp(function(err,result){
            console.info(result);
        });


        var industryIds = {
            "industry_id1":'1',
            "industry_id2":"4"
        };
        api.setIndustry(industryIds, function(err,data){
            if(err) {
                console.info("err:" + err);
            }
            console.info(data);

        });

        /*var templateIdShort = 'OPENTM207327376';
         api.addTemplate(templateIdShort, function(err,data){
         if(err) {
         console.info("err:" + err);
         }
         var templateId = data.template_id;
         // URL置空，则在发送后,点击模板消息会进入一个空白页面（ios）, 或无法点击（android）
         var url= 'http://weixin.qq.com/download';
         var topcolor = '#FF0000'; // 顶部颜色
         var data = {
         first:{
         "value":'尊敬的会员你好，卡券已经兑换成功。',
         "color":"#173177"
         },
         keyword1:{
         "value":'30元',
         "color":"#173177"
         },
         keyword2:{
         "value":'30元',
         "color":"#173177"
         },
         keyword3:{
         "value":'30元',
         "color":"#173177"
         },
         keyword4:{
         "value":'长期有效',
         "color":"#173177"
         },
         remark : {
         "value":'感谢你的使用或直接在微信留言，小易将第一时间为您服务！',
         "color":"#173177"
         }
         };
         api.sendTemplate(req.session.wuser.openid, templateId, url, topcolor, data, function(err,data){
         if(err) {
         console.info("err:" + err);
         }
         console.info(data);
         });
         });

         var industryIds = {
         "industry_id1":'1',
         "industry_id2":"4"
         };
         api.setIndustry(industryIds, function(err,data){
         if(err) {
         console.info("err:" + err);
         }
         console.info(data);

         });

         var templateIdShort = 'TM00015';
         api.addTemplate(templateIdShort, function(err,data){
         if(err) {
         console.info("err:" + err);
         }
         console.info(data);


         var templateId = data.template_id;
         // URL置空，则在发送后,点击模板消息会进入一个空白页面（ios）, 或无法点击（android）
         var url= 'http://weixin.qq.com/download';
         var topcolor = '#FF0000'; // 顶部颜色
         var data = {
         orderMoneySum:{
         "value":'30元',
         "color":"#173177"
         },
         orderProductName:{
         "value":'我是商品名字',
         "color":"#173177"
         },
         remark : {
         "value":'如有问题请致电400-828-1878或直接在微信留言，小易将第一时间为您服务！',
         "color":"#173177"
         }
         };
         api.sendTemplate(req.session.wuser.openid, templateId, url, topcolor, data, function(err,data){
         if(err) {
         console.info("err:" + err);
         }
         console.info(data);
         });
         });*/
    })

    app.post('/setFav/:_id', checkLogin);
    app.post('/setFav/:_id', function(req, res) {
        var _id = req.params._id;
        var arg = url.parse(req.url).query;
        var type = querystring.parse(arg).action;
        WUser.setFav(req.session.wuser.openid,_id,type,function(err){
            if (err) {
                return res.redirect('back');
            }
            res.end("success");
        })
    });

    app.get('/favlist', checkLogin);
    app.get('/favlist', function(req, res) {
        WUser.get(req.session.wuser.openid,function(err, wuser){
            Post.favArchive(wuser.fav,function (err, posts) {
                if (err) {
                    return res.redirect('/');
                }
                res.render('user/favlist', {
                    title: '我的关注',
                    posts: posts
                });
            });
        });
    })


    app.get('/case', function(req, res) {
        var arg = url.parse(req.url).query;
        var type = querystring.parse(arg).debug;
        console.info(type);
        if(type) {
            WUser.get("sdfadfa1231231231",function(err, wuser){
                if(err || wuser == null){
                    var _user = new WUser({
                        openid: "sdfadfa1231231231",
                        nickname: "苟建军杀敌发到付",
                        headimgurl: "http://bs.sd188.cn/img/profile_small.jpg",
                        city: "Chengdu",
                        province: "Sichuan",
                        country: "China",
                        sex: 1
                    });
                    _user.save(function(err, wuser) {
                        if (err) {
                            console.log('User save error ....' + err);
                        } else {
                            console.log('User save sucess ....' + err);
                            req.session.wuser = wuser;
                        }
                    });
                }
            });
        }
        Post.getArchive(function (err, posts) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }

            Post.getBanner(function (err, banner) {
                if (err) {
                    req.flash('error', err);
                    return res.redirect('/');
                }
                res.render('cases', {
                    title: '公益中心',
                    posts: posts,
                    banner: banner
                });
            });

        });
    });
    app.get('/p/:_id', function(req, res){
        var _id = req.params._id;
        var isFav = false;
        console.info(req.session.wuser);
        Post.getOne(_id, function (err, post) {
            if (err) {
                return res.redirect('/');
            }
            //未登录
            if(req.session.wuser === undefined) {
                return res.render('cases/casedetail', {
                    isFav : false,
                    title: post.title,
                    post: post,
                    isFav : null
                });
            }
            WUser.get(req.session.wuser.openid,function(err, wuser){
                wuser.fav.forEach(function(i){
                    if(i === _id) isFav = true;
                });
                res.render('cases/casedetail', {
                    user : req.session.wuser,
                    isFav : isFav,
                    title: post.title,
                    post: post
                });
            });

        });
    });

    app.get('/card', checkLogin);
    app.get('/card', function (req, res) {
        Card.getArchive(function (err, posts) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            WUser.get(req.session.wuser.openid,function(err, wuser){
                res.render('cards', {
                    title: '兑换',
                    posts: posts,
                    wuser : wuser
                });
            });

        });
    });

    app.get('/mysupports', checkLogin);
    app.get('/mysupports', function (req, res) {
        WUser.get(req.session.wuser.openid,function(err, wuser){
            Post.supportArchive(wuser.support,function (err, posts) {
                if (err) {
                    return res.redirect('/');
                }
                res.render('user/mysupports', {
                    title: '我的支持',
                    posts: posts
                });
            });
        });
    });

    app.get('/mycards', checkLogin);
    app.get('/mycards', function (req, res) {
        WUser.get(req.session.wuser.openid,function(err, wuser){
            console.info(wuser.card);
            Card.getCards(wuser.card,function (err, posts) {
                if (err) {
                    return res.redirect('/');
                }
                res.render('user/mycards', {
                    title: '我的卡券',
                    posts: posts,
                    cardstotal : genArrObj(wuser.card)
                });
            });
        });
    });

    app.get('/card/:_id', checkLogin);
    app.get('/card/:_id', function(req, res){
        Card.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            WUser.get(req.session.wuser.openid,function(err, wuser){
                var isBuy =  post.point < wuser.point;
                res.render('cards/carddetail', {
                    title: post.title,
                    post: post,
                    wuser: wuser,
                    isBuy :  isBuy
                });
            });

        });
    });

    app.get('/mycard/:_id', checkLogin);
    app.get('/mycard/:_id', function(req, res){
        Card.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            WUser.get(req.session.wuser.openid,function(err, wuser){
                var isBuy =  post.point < wuser.point;
                res.render('user/mycard', {
                    title: post.title,
                    post: post,
                    wuser: wuser,
                    isBuy :  isBuy
                });
            });

        });
    });

    app.post('/exchange/:_id', checkLogin);
    app.post('/exchange/:_id', function(req, res) {
        var _id = req.params._id;
        Card.exchange(req.session.wuser.openid,_id,function(err,doc){
            if (err) {
                return res.redirect('back');
            }
            res.end("success");
        })

    });

    app.post('/useCard/:_id', checkLogin);
    app.post('/useCard/:_id', function(req, res) {
        var _id = req.params._id;
        Card.getOne(_id, function (err, card) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            WUser.get(req.session.wuser.openid,function(err, wuser){
                //删除用户使用的卡券
                var cardtotal = delArrItem(wuser.card,_id);
                WUser.useCard(req.session.wuser.openid,cardtotal,function(err){
                    if (err) {
                        return res.redirect('back');
                    }
                })

                var url= 'http://www.sd188.cn/card/' + _id;
                var topcolor = '#FF0000'; // 顶部颜色
                var data = {
                    first:{
                        "value":'尊敬的会员你好，卡券已经兑换成功。',
                        "color":"#173177"
                    },
                    keyword1:{
                        "value":card.title,
                        "color":"#173177"
                    },
                    keyword2:{
                        "value":card.point,
                        "color":"#173177"
                    },
                    keyword3:{
                        "value":wuser.point,
                        "color":"#173177"
                    },
                    keyword4:{
                        "value":'长期有效',
                        "color":"#173177"
                    },
                    remark : {
                        "value":'感谢你的使用！',
                        "color":"#173177"
                    }
                };
                api.sendTemplate(req.session.wuser.openid, "CcTnIqlFTDH6WcZWhfIYbHjJigZpsn9GudZ8Nzi3kXI", url, topcolor, data, function(err,data){
                    if(err) {
                        console.info("err:" + err);
                    }
                });
            });

            res.end("success");

        });
    });

    app.get('/comment/:_id', checkLogin);
    app.get('/comment/:_id', function (req, res) {
        Post.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            res.render('cases/comment', {
                title: post.title
            });
        });
    });

    app.post('/comment/:_id', checkLogin);
    app.post('/comment/:_id', function (req, res) {
        var _id = req.params._id;
        var newComment = new Comment(req.session.wuser.nickname,req.body.comment,req.session.wuser.headimgurl);
        newComment.save(_id,function (err) {
            if (err) {
                return res.redirect('back');
            }
            res.redirect('/p/' + _id);
        });
    });

    app.get('/my', checkLogin);
    app.get('/my', function(req, res) {
        WUser.get(req.session.wuser.openid,function(err, wuser){
            res.render('user', {
                title: '个人中心',
                wuser : wuser,
                supportlen : unique(wuser.support).length
            });
        });
    });

    app.get('/logintip', function(req, res){
        res.render('logintip', {
            title: '请用微信登录'
        });
        res.redirect('/mall');
    });

    function checkLogin(req, res, next) {
        console.info("checkLogin end:");
        console.info(req.session.wuser);
        if (!req.session.wuser) {
            return res.redirect('/logintip');  //一定要return 不然报错 “Can't set headers after they are sent.”
        }
        next();
    }

    function checkNotLogin(req, res, next) {
        if (req.session.wuser) {
            return res.redirect('back');
        }
        next();
    }

    function genArrObj(arr){
        var obj = {};
        for(var i=0;i<arr.length; i++) {
            if(obj[arr[i]]  == undefined) {
                obj[arr[i]] = 1
            } else {
                obj[arr[i]]++;
            }
        };
        return obj;
    }
    function unique(arr) {
        var result = [], hash = {};
        for (var i = 0, elem; (elem = arr[i]) != null; i++) {
            if (!hash[elem]) {
                result.push(elem);
                hash[elem] = true;
            }
        }
        return result;
    }
    function delArrItem (arr,dx) {
        for(var i=0;i<arr.length; i++) {
            if(arr[i] == dx) {
                arr.splice(i,1)
                break;
            }
        };
        return arr;
    }

    function CurentTime()
    {
        var now = new Date();

        var year = now.getFullYear();       //年
        var month = now.getMonth() + 1;     //月
        var day = now.getDate();            //日

        var clock = year;

        if(month < 10)
            clock += "0";

        clock += month;

        if(day < 10)
            clock += "0";

        clock += day;
        return(clock);
    }
}